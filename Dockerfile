FROM alpine:latest

WORKDIR app

COPY hello_world.sh hello_world.sh

ENTRYPOINT ["sh", "hello_world.sh"]